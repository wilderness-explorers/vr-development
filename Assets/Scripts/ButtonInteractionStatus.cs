using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonInteractionStatus : MonoBehaviour
{
    public TextMeshProUGUI statusText;

    public void OnButton1Clicked()
    {
        statusText.text = "Button 1 is clicked.";
    }

    public void OnButton2Clicked()
    {
        statusText.text = "Button 2 is clicked.";
    }

    public void OnButton3Clicked()
    {
        statusText.text = "Button 3 is clicked.";
    }
}
